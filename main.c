//
//  main.c
//  bbsh
//
//  bbsh_1.0 (Bruno Balic SHell)
//
//  Created by Bruno Balic on 18/01/2020.
//  Copyright © 2020 Bruno Balic. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <dirent.h>
#include <termios.h>

int bbsh_help(char** args);
int bbsh_exit(char** args);
int bbsh_pwd(char** args);
int bbsh_cd(char** args);
int bbsh_ls(char** args);

int bbsh_history(char** args);

char* builtin_list_str[] = {
    "help",
    "exit",
    "pwd",
    "cd",
    "history",
    "ls"
};

int (*builtin_func[]) (char**) = { // deklaracija funkcija, pointer na funkciju, a ovo (char**) oznacava tip parametra koji prima... ovaj nacin mogu koristiti samo sa funkcijama koje imaju isti return type i ulazne parametre...
    &bbsh_help,
    &bbsh_exit,
    &bbsh_pwd,
    &bbsh_cd,
    &bbsh_history,
    &bbsh_ls
};

// broj koliko ima builtin funkcija
int builtin_count() {
    return sizeof(builtin_list_str) / sizeof(char*);
}

/********************************/
// implementacija builtin funkcija

int bbsh_help(char** args) {
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    printf("//\n");
    printf("//  bbsh - Shell made by Bruno Balic\n");
    printf("//\n");
    printf("//  Builtin commands:\n");
    for (int i = 0; i < builtin_count(); i++) {
        printf(" %d. :  %s\n", i + 1, builtin_list_str[i]);
    }
    printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    return 1;
}

int bbsh_exit(char** args) {
    return 0;
}

int bbsh_pwd(char** args) {
    char buffer[256];
    printf("%s\n", getcwd(buffer, 256));
    return 1;
}

int bbsh_cd(char** args) {
    if (args[1] == NULL) {
        fprintf(stderr, "bbsh: \"cd\" error, expected argument\n");
    }
    else {
        if (chdir(args[1]) != 0) {
            perror("bbsh");
        }
    }
    return 1;
}

int bbsh_history(char** args) {
    return 1;
}

int bbsh_ls(char** args) {
    struct dirent* de;
    
    DIR *dr = opendir(".");
    
    if (dr == NULL) {
        printf("Could not open current directory");
        return 1; // ovo triba vodit jel triba 1
    }
    
    while ((de = readdir(dr)) != NULL) {
        
        if (de->d_type == DT_DIR) {
            printf("d ");
        }
        else {
            printf("- ");
        }
        printf("%s\n", de->d_name);
        
    }
    
    closedir(dr);
    return 1;
}

// implementacija builtin funkcija - END
/********************************/

/********************************/
// pomocne funkcije

// samo trenutni direktorj, ne cijela putanja
char* get_name_of_current_dir(char* tmp_buffer) {
    char buffer[256];
    getcwd(buffer, 256);
    
    if (strcmp(buffer, "/") == 0) {
        tmp_buffer = malloc(sizeof(char) * 2);
        tmp_buffer[0] = '/';
        tmp_buffer[1] = '\0';
        return tmp_buffer;
        //return "/";
    }
    
    tmp_buffer = malloc(sizeof(char) * 256);
    int possition = 0;
    for (int i = 1; i < strlen(buffer); i++) {
        if (buffer[i] == '/') {
            possition = i;
        }
    }
    
    int i = 0;
    for (; (i + possition) < strlen(buffer); i++) {
        tmp_buffer[i] = buffer[possition + i + 1];
    }
    tmp_buffer[i] = '\0';
    
    return tmp_buffer;
}

// samo trenutni direktorj, ne cijela putanja
void print_current_dir(void) {
    char buffer[256];
    getcwd(buffer, 256);
    
    if (strcmp(buffer, "/") == 0) {
        printf("/");
        return;
    }
    
    int possition = 0;
    for (int i = 1; i < strlen(buffer); i++) {
        if (buffer[i] == '/') {
            possition = i;
        }
    }
    
    int i = 0;
    for (; (i + possition) < strlen(buffer); i++) {
        printf("%c", buffer[possition + i + 1]);
    }
    printf("%c", '\0');
    return;
}

// pomocne funkcije - END
/********************************/

/********************************/
// bbsh interne funkcije

struct bbsh_history {
    char** history_list;
    int history_list_size;
    int history_list_index;
};

struct bbsh_history create_history(void) {
    struct bbsh_history h;
    h.history_list_size = 128;
    h.history_list_index = 0;
    h.history_list = malloc(h.history_list_size * sizeof(char*));
    return h;
}

void add_to_history(struct bbsh_history* history, char* line) {
    // realokacija ako ponestane mijesta
    if (history->history_list_index >= history->history_list_size) {
        history->history_list_size *= 2;
        history->history_list = realloc(history->history_list, (history->history_list_size * sizeof(char*)));
    }
    
    // dodavaje u history
    history->history_list[history->history_list_index] = malloc(sizeof(char) * strlen(line) + 1);
    memcpy(history->history_list[history->history_list_index], line, strlen(line) + 1);
    history->history_list_index++;
}

void print_history(struct bbsh_history* history) {
    for (int i = 0; i < history->history_list_index; i++) {
        printf("%s\n", history->history_list[i]);
    }
}

void free_history(struct bbsh_history* history) {
    for (int i = 0; i < history->history_list_index; i++) {
        free(history->history_list[i]);
    }
    free(history->history_list);
}

// bbsh interne funkcije - END
/********************************/

int bbsh_launch(char** args) {
    pid_t pid;
    pid_t wpid;
    
    int status;
    
    pid = fork();
    
    if (pid == 0) {
        // child process
        if (execvp(args[0], args) == -1) {
            perror("bbsh");
        }
        exit(EXIT_FAILURE);
    }
    else if (pid < 0) {
        // error forking
        perror("bbsh");
    }
    else {
        // Parent process
        do {
            wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    
    return 1;
}

int bbsh_execute(char** args) {
    if (args[0] == NULL) {
        // empty command
        //printf("\n");
        return 1;
    }
    
    for (int i = 0; i < builtin_count(); i++) {
        if (strcmp(args[0], builtin_list_str[i]) == 0) {
            // ako je builtin funckija
            return (*builtin_func[i])(args);
        }
    }
    
    // ako nije builtin funkcija
    return bbsh_launch(args);
}

char** bbsh_split_line(char* line) {
    unsigned int token_buffer_size = 64;
    char token_delimiter[] = " \t\r\n\a";
    
    int buffer_size = token_buffer_size;
    int position = 0;
    
    char** tokens_array = malloc(buffer_size * sizeof(char*));
    if (tokens_array == NULL) {
        fprintf(stderr, "bbsh: split_line malloc error\n");
        exit(EXIT_FAILURE);
    }
    
    char* token;
    token = strtok(line, token_delimiter);
    while (token != NULL) {
        tokens_array[position] = token;
        position++;
        
        // realokacija buffera ako nema vise memorije
        if (position >= buffer_size) {
            //buffer_size += BBSH_TOKEN_BUFFER_SIZE;
            buffer_size += token_buffer_size;
            tokens_array = realloc(tokens_array, (buffer_size * sizeof(char*)));
            if (tokens_array == NULL) {
                fprintf(stderr, "bbsh: split_line malloc error\n");
                exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, token_delimiter);
    }
    tokens_array[position] = NULL;
    return tokens_array;
}

//char getch_test(void) {
char getch_test(struct bbsh_history* history) {
    
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0) {
        perror("tcgetattr()");
    }
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1; // unsigned char
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0) {
        perror("tcsetattr ICANON");
    }
    /*
    if (read(0, &buf, 1) < 1) { // STDIN_FILENO = 0
        perror("read()");
    }
     
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror("tcsetattr ~ICANON");
    }
     
    printf("%c\n", buf);
    //printf("\n");
    return buf;
    */
    
    
    char buffer_full[1024]; // samo za testiranje, treba alocirato memoriju
    int buffer_position = 0;
    
    //int user_input_chars = 0;
    
    int current_histoty_index = history->history_list_index;
    
    char buf = 0;
    
    while (read(0, &buf, 1) > 0) {
        
        // ENTER, kraj upisa, return buffer_full
        if (buf == '\n') { // ?? ascii 10
        //if (buf == 10) { // ascii 10, lf, line feed
            buffer_full[buffer_position] = buf;
            //buffer_position--; // jel mi ovo treba??, mislim da mi ne treba
            break;
        }
        else if (buf == 127) { // DEL, ascii decimalna vrijednost za backspace
            if (buffer_position > 0) { // ako imamo vise 1 znaka
                buffer_position--;
                printf("\b \b"); //
            }
        }
        else if (buf == 27) {
            //buffer_full[buffer_position] = buf;
            //buffer_position++;
            read(0, &buf, 1); // cita 2.
            
            if (buf == 91) {
                //buffer_full[buffer_position] = buf;
                //buffer_position++; // cita 3.
                read(0, &buf, 1);
                
                if (buf == 65) { // strelica prema gore ptitisnuta
                    //buffer_position -= 2; // izbaci 27, izbaci 21
                    
                    //if (history->history_list_index > 0) { // ako postoji history
                    if (current_histoty_index > 0) { // ako postoji history
                        
                        
                        // "brisanje" svega do sada
                        for (int i = 0; i < buffer_position; i++) {
                            printf("\b \b");
                        }
                        buffer_position = 0;
                        
                        for (int i = 0; i < strlen(history->history_list[current_histoty_index - 1]); i++) { // history treba biti terminiran, da bi strlen bio dobar
                            buffer_full[i] = history->history_list[current_histoty_index - 1][i];
                            buffer_position++;
                        }
                        current_histoty_index--;
                        
                        
                        //buffer_full[buffer_position] = '\n';
                        //buffer_position++;
                        
                        
                        //printf("%s", buffer_full);
                        for (int i = 0; i < buffer_position; i++) {
                            printf("%c", buffer_full[i]);
                        }
                        
                    }
                    // else, nista, zanemari botun
                }
            
            }
            
        }
        // neka ovaj else bude za sve ascii koji se mogu printati
        // napisati if uvjet
        else {
            buffer_full[buffer_position] = buf;
            buffer_position++;
            
            printf("%c", buf);
        }
        
        //printf("%c", buf);
        
    }
    //printf("\n");
    for (int i = 0; i < buffer_position; i++) {
        //printf("%d\n", buffer_full[i]);
    }
    
    for (int i = 0; i < buffer_position; i++) {
        //printf("%c", buffer_full[i]);
    }
    
    //printf("\n%s", buffer_full); // necemo ovako printati buffer_full
    printf("\n");
    for (int i = 0; i < buffer_position; i++) {
        printf("%c", buffer_full[i]);
    }
    
    
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0) {
        perror("tcsetattr ~ICANON");
    }
    
    //printf("%c\n", buf);
    printf("\n");
    return buf;
}

char* bbsh_read_line(struct bbsh_history* history) {
    // konfiguracija terminala
    struct termios old_termios = {0};
    if (tcgetattr(0, &old_termios) < 0) {
        perror("tcgetattr()");
    }
    old_termios.c_lflag &= ~ICANON;
    old_termios.c_lflag &= ~ECHO;
    old_termios.c_cc[VMIN] = 1; // unsigned char
    old_termios.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old_termios) < 0) {
        perror("tcsetattr ICANON");
    }
    
    int read_line_buffer_size = 1024;
    int buffer_size = read_line_buffer_size;
    
    char* buffer = malloc(buffer_size * sizeof(char));
    if (buffer == NULL) {
        fprintf(stderr, "bbsh: read_line malloc error\n");
        exit(EXIT_FAILURE);
    }
    int buf_position = 0;
        
    int current_history_index = history->history_list_index;
    
    char ch_in;
    int cursor_offset = 0; // nikad nije veci od 0
    
    char cursor_insert_buffer[512];
    //int cursor_insert_buffer_size = 512;
    int cursor_insert_buffer_position = 0; // pokazuje na slobodno mijesto
    
    char active_line_holder[1024]; // ovo koristim samo kada setam po history-u, da sacuvam aktualnu liniju
    int active_line_position = 0;
    
    while (read(STDIN_FILENO, &ch_in, 1) > 0) { // STDIN_FILENO == 0, oznacava stdin
        
        // ENTER
        if (ch_in == '\n') { // ascii dec == 10
            //buffer[buf_position] = ch_in; // bolje je da ne dodajem \n, zbog toga kada prelistavam heistory sa strelicama
            //buf_position++;
            buffer[buf_position] = '\0';
            buf_position++;
            printf("\n"); // nova linija nakon zavrsenog unosa
            break;
        }
        
        // DEL, ascii == 127
        // backspace, brisanje 1 char
        else if (ch_in == 127) {
            
            // brisemo ako postoji vise od 1 znak u bufferu
            if (buf_position > 0) {
                
                // za brisanje u "sredini" linije
                if (cursor_insert_buffer_position > 0) {
                    printf("\b"); // ovdje nema potrebe za "\b \b", jer ce se svakako napisati neki znak iza
                    buf_position--;
                    
                    for (int i = 0; i < cursor_insert_buffer_position; i++) {
                        
                        buffer[buf_position + cursor_offset + i] = cursor_insert_buffer[(cursor_insert_buffer_position - 1) - i];
                        
                        printf("%c", cursor_insert_buffer[(cursor_insert_buffer_position - 1) - i]);
                        
                    }
                    printf(" \b"); // brisanje najdesnijeg znaka (da mi se ne vidi u liniji u konzoli)
                    
                    for (int i = 0; i < cursor_insert_buffer_position; i++) {
                        
                        printf("\033[1D"); // micem cursor nazad lijevo
                        
                    }
                    
                }
                
                else {
                    //printf("\b"); ako ovo napravim, brisem (CURSOR SE POMICE KAO POSLJEDICA);
                    // \b ce i pomaknuti cursor
                    printf("\b \b"); // brisanje jednog chara, pisanje razmaka, brisanje razmaka (bez razmaka ostaje prikazan izbrisani znak, zato space)
                    buf_position--; // nista zapravo ne brisemo iz samog buffera, samo smanjimo buf_position
                }
                
            }
            
        }
        
        // ESC, ascii == 27, oct == \033
        else if (ch_in == 27) {
            read(STDIN_FILENO, &ch_in, 1);
            
            // [, ascii == 91
            if (ch_in == 91) {
                read(STDIN_FILENO, &ch_in, 1);
                
                // A, ascii == 65
                // strelica gore
                if (ch_in == 65) {
                    
                    //printf("UP current_history_index: %d\n", current_history_index);
                    
                    
                    // spremanje aktualne linije
                    if (current_history_index == history->history_list_index) {
                        for (int i = 0; i < buf_position; i++) {
                            active_line_holder[i] = buffer[i];
                            active_line_position++;
                        }
                    }
                    
                    
                    // ako postoje naredbe u history-u
                    if (current_history_index > 0) {
                        
                        //micanje cursora skroz desno, (kad bi htio da mi cursor ostane na istom mijestu, trebao bi izracunati cursor_offser za ovo novu naredbu if history-a)
                        for (int i = 0; i < cursor_insert_buffer_position; i++) {
                            printf("\033[1C");
                        }
                        cursor_insert_buffer_position = 0;
                        cursor_offset = 0;
                        
                        // "brisanje" svega do sada sa ekrana
                        for (int i = 0; i < buf_position; i++) {
                            printf("\b \b");
                        }
                        buf_position = 0;
                        
                        // upisivanje u buffer
                        for (int i = 0; i < strlen(history->history_list[current_history_index - 1]); i++) { // pod uvjetom da je string u history-u terminiran
                            buffer[i] = history->history_list[current_history_index - 1][i];
                            buf_position++;
                        }
                        current_history_index--; // mogao sam ga umanjiti i prije for petlje
                        
                        // ispisivanje na ekran
                        for (int i = 0; i < buf_position; i++) {
                            printf("%c", buffer[i]);
                        }
                        
                    }
                    
                }
                
                // B, ascii == 66
                // strelica dole
                else if (ch_in == 66) {
                    
                    //printf("DOWN current_history_index: %d\n", current_history_index);
                    
                    // ako je manji znaci da smo vec barem jednom pritisnuli strelicu prema gore
                    if (current_history_index < history->history_list_index) {
                        
                        //micanje cursora skroz desno, (kad bi htio da mi cursor ostane na istom mijestu, trebao bi izracunati cursor_offser za ovo novu naredbu if history-a)
                        for (int i = 0; i < cursor_insert_buffer_position; i++) {
                            printf("\033[1C");
                        }
                        cursor_insert_buffer_position = 0;
                        cursor_offset = 0;
                        
                        // "brisanje" svega do sada sa ekranu
                        for (int i = 0; i < buf_position; i++) {
                            printf("\b \b");
                        }
                        buf_position = 0;
                        
                        // znaci da se radi o najnovijoj liniju koju tek upisujeme (nije jos u history-u)
                        if (history->history_list_index - current_history_index == 1) { // ==1 ili ==0
                            // treba napraviti da pamti najnoviju (neunesenu liniju)
                            //printf("");
                            
                            for (int i = 0; i < active_line_position; i++) {
                                buffer[i] = active_line_holder[i];
                                buf_position++;
                            }
                            
                            current_history_index++;
                            
                        }
                        
                        // ispisujemo liniju iz history-a
                        else {
                            
                            for (int i = 0; i < strlen(history->history_list[current_history_index + 1]); i++) { // ??? current_history_index + 1
                                
                                buffer[i] = history->history_list[current_history_index + 1][i];
                                buf_position++;
                                
                            }
                            
                            current_history_index++; // mogao sam ga uvecati i prije for petlje
                            
                        }
                        
                        // ispisivanje na ekran
                        for (int i = 0; i < buf_position; i++) {
                            printf("%c", buffer[i]);
                        }
                        
                    }
                    
                }
                
                // D, ascii == 68
                // strelica lijevo
                else if (ch_in == 68) {
                    
                    // da bi ostali u vizualnim "granicama"
                    if (buf_position > 0 && cursor_offset > (-buf_position)) {
                        printf("\033[1D"); // pomice cursor u lijevo 1 put (pomice onoliko puta koliki je broj)
                        
                        cursor_offset--;
                        
                        cursor_insert_buffer[cursor_insert_buffer_position] = buffer[buf_position + cursor_offset]; // cursor_offset je u minusu
                        cursor_insert_buffer_position++;
                        
                    }
                    
                }
                
                // C, ascii == 67
                // strelica desno
                else if (ch_in == 67) {
                    
                    // da bi ostali u vizualnim "granicama"
                    if (cursor_offset < 0) {
                        printf("\033[1C"); // pomice cursor u desno 1 put (pomice onoliko puta koliki je broj)
                        
                        cursor_offset++;
                        
                        cursor_insert_buffer_position--;
                        
                    }
                    
                }
                
            }
            
        }
        
        // ascii znakovi koji se mogu printati, 32 == Space , 126 == ~
        else if (ch_in >= 32 && ch_in <= 126) {
            
            if (cursor_insert_buffer_position > 0) {
                
                buffer[buf_position + cursor_offset] = ch_in;
                buf_position++;
                printf("%c", ch_in);
                
                for (int i = 0; i < cursor_insert_buffer_position; i++) {
                    
                    buffer[buf_position + cursor_offset + i] = cursor_insert_buffer[(cursor_insert_buffer_position - 1) - i];
                    
                    printf("%c", cursor_insert_buffer[(cursor_insert_buffer_position - 1) - i]);
                    
                }
                
                for (int i = 0; i < cursor_insert_buffer_position; i++) {
                    
                    printf("\033[1D"); // micem cursor nazad lijevo
                    
                }
                
            }
            
            else {
                buffer[buf_position] = ch_in;
                buf_position++;
                printf("%c", ch_in);
            }
            
        }
        
    } // while OVER
    
    // vracanje postavki terminala na staro
    old_termios.c_lflag |= ICANON;
    old_termios.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old_termios) < 0) {
        perror("tcsetattr ~ICANON");
    }
    
    return buffer;
}


void bbsh_loop(void) {
    char* line;
    char** args;
    
    struct bbsh_history history1 = create_history();
    
    //setbuf(stdout, NULL);
    setvbuf(stdout, NULL, _IONBF, 0); // iskljucujem buffer kod stdout printanja, (printa bi mi nesto tek kada se read() odradi...)
    
    int status = 1;
    while (status) {
        printf(">");
        char hostname[256];
        if (gethostname(hostname, 256) == 0) {
            printf("%s:", hostname); // hostname
        }
        print_current_dir();
        printf(" %s", getlogin()); // username
        printf("$ ");
        
        line = bbsh_read_line(&history1);
        
        //printf("%lu\n", strlen(line));
        if (strlen(line) > 0) {
            // line_tmp da mi spremi cijelu naredbu, (ne koristim line jer se razdvoji na tokene..., pa ne ispadne uvijek dobro)
            char* line_tmp = malloc((strlen(line) + 1) * sizeof(char));
            memcpy(line_tmp, line, (strlen(line) + 1));
            
            args = bbsh_split_line(line);
            
            // ako je naredba "history", izvrsim naredbu history
            if (args[0] != NULL) {
                if (strcmp(args[0], "history") == 0) {
                    print_history(&history1);
                }
                // ako nije naredba "history", dodajem liniju u history
                else { // ne dodajem "history" naredbu u history
                    add_to_history(&history1, line_tmp);
                }
            }
            
            free(line_tmp);
            
            // print args
            /*
            for (int j = 0; args[j] != NULL; j++) {
                printf("args[%d]: %s\n", j, args[j]);
            }
            */
            
            status = bbsh_execute(args);
            free(args);
        }
        free(line);
    }
    free_history(&history1);
}

int main(int argc, char * argv[]) {
    
    bbsh_loop();
    
    return EXIT_SUCCESS;
}
//
